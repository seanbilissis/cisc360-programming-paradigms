% CISC 360 a4, Winter 2021
%
% See a4.pdf for instructions

/*
 * Q1: Student ID
 */
student_id(20004468).

/*
 * Q2: Prime numbers
 */
 
/*
  hasFactor(N, F): Given positive integers N and F,
                   true iff N has the factor F, that is,
                   N is divisible by F.
*/
 
hasFactor(N, F) :- N mod F =:= 0.
hasFactor(N, F) :- N mod F =\= 0,
                   F * F < N,
                   Fplus1 is F + 1,
                   hasFactor(N, Fplus1).

/*
  isPrime(N, What)

  Given an integer N >= 2:
    What = prime      iff  N is prime
    What = composite  iff  N is composite
*/

isPrime(2, prime).
isPrime(N, composite) :- N > 2,    hasFactor(N, 2).
isPrime(N, prime)     :- N > 2, \+ hasFactor(N, 2).


/*
  findPrimes(Numbers, Primes)
    Primes = all prime numbers in Numbers
*/

findPrimes([], []).

findPrimes([X | Xs], [X | Ys]) :-
    isPrime(X, prime), 
    findPrimes(Xs, Ys).

findPrimes([X | Xs], Ys) :-
    isPrime(X, composite), 
    findPrimes(Xs, Ys).

/*
  upto(X, Y, Zs):
  Zs is every integer from X to Y

  Example:
     ?- upto(3, 7, Range)
     Range = [3, 4, 5, 6, 7]
*/

upto(X, X, [X]).
upto(X, Y, [X | Zs]) :-
    X < Y,
    Xplus1 is X + 1,
    upto(Xplus1, Y, Zs).

/*
  primes_range(M, N, Primes)
    Primes = all prime numbers between M and N
    Example:
      ?- primes_range(60, 80, Primes).
      Primes = [61, 67, 71, 73, 79] .
*/

primes_range(M, N, Primes) :-
    upto(M, N, Range), 
    findPrimes(Range, Primes).


/*
 * Q3. Translate the tower function from a1.

In Haskell, div (n + k) k
            returns the "floor" of (n + k) divided by k.

In Prolog, this operation is also named "div":
   ?- X is 11 div 2.
   X = 5.

tower(N, K, R) true  iff  R = (tower N K)
*/

tower(N, K, 1) :- 
    K > N.

tower(N, K, R) :- 
    K =< N,
    Kplus1 is K + 1,
    tower(N, Kplus1, Towerresult),
    R is (N + K + 1) div K * Towerresult.

/*
  To test: ?- tower(5, 6, 1).
           true .
           ?- tower(5, 5, 2).
           true .
           ?- tower(7, 4, R).
           R = 24 .             % type .
           ?- tower(14, 4, R).
           R = 18432            % type ;
           false.

  Hint: 
    If you get more than one solution when you type ;,
    look at how we defined hasFactor.
*/


/* Q4: Trees */

inorder(leaf(K), [K]).

inorder(node(K, L, R), Keys) :-
  inorder(L, Lkeys),
  inorder(R, Rkeys),
  append(Lkeys, [K | Rkeys], Keys).

/*
Q4a. Define a predicate

  postorder(T, Keys)

that is true iff, given a tree T, its *post-order* traversal is Keys.

In a post-order traversal, we visit the left subtree, the right subtree,
and *then* the root.

For example, you should get

  ?- postorder( node( 4, node( 2, leaf(1), leaf(3)), leaf(5)), Keys).
  Keys = [1, 3, 2, 5, 4]
*/

postorder(leaf(K), [K]).

postorder(node(K, L, R), Keys) :- 
    postorder(L, Lkeys),
    postorder(R, Rkeys),
    append(Lkeys, Rkeys, LRkeys),
    append(LRkeys, [K], Keys).



/*
Q4b.  In-order and post-order traversals are paths through the entire tree.
  In this part of the question, we consider "vertical" paths:
  from the root to a leaf.
  
  In the tree above (leaves 1, 3, 5),
  the paths starting from the root are:

    4 to 2 to 1          [4, 2, 1]
    4 to 2 to 3          [4, 2, 3]
    4 to 5               [4, 5]

  The above tree can be represented in Prolog as

    node(4, node(2, leaf(1), leaf(3)), leaf(5))

  In this question, define a Prolog predicate

    findpath(Tree, Path)

  such that if we start from the root of Tree,
  then Path is a list of numbers from the root to a leaf.

  For example:
  
    ?- findpath(node(2, leaf(1), leaf(3)), [2, 1]).
    true
    ?- findpath(node(2, leaf(1), leaf(3)), [2, 3]).
    true

  Your predicate should be written so that when the first argument is a specific tree
  (containing no variables) and the second argument is a variable, typing ; returns
  *all* possible paths to all the leaves, from left to right.  For example:

    ?- findpath(node(4, node(2, leaf(1), leaf(3)), leaf(5)), Path).
    Path = [4, 2, 1]
    Path = [4, 2, 3]
    Path = [4, 5].

  Hint:
    ?- findpath(leaf(2), [2]).                       2      [2]
  should be true.

  Finish defining clauses for findpath below.
*/

findpath(leaf(K), [K]).

findpath(node(K, L, _), Path) :-
    findpath(L, Lpath),
    Path = [K | Lpath].

findpath(node(K, _, R), Path) :-
    findpath(R, Rpath),
    Path = [K | Rpath].